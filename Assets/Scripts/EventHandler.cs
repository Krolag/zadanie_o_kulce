﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventHandler : MonoBehaviour
{
    [SerializeField] Text _first, _second, _third;
    private bool isOn;

    private void Awake()
    {
        _first.color = Color.red;
        _second.color = Color.red;
        _third.color = Color.red;
        isOn = false;
    }

    public void FirstEvent()
    {
        if (_first.color == Color.red)
        {
            _first.color = Color.green;
            _first.fontSize = 15;
        }
        else
        {
            _first.color = Color.red;
            _first.fontSize = 24;
        }
    }

    public void SecondEvent()
    {
        if (_second.color == Color.red)
        {
            _second.color = Color.green;
            _second.fontStyle = FontStyle.BoldAndItalic;
        }
        else
        {
            _second.color = Color.red;
            _second.fontStyle = FontStyle.Normal;
        }
    }

    public void ThirdEvent()
    {
        if (_third.color == Color.red)
        {
            _third.color = Color.green;
            _third.text = "Event third";
        }
        else
        {
            _third.color = Color.red;
            _third.text = "Third Event";
        }
    }

    public void OnAndOff()
    {
        if (!isOn)
        {
            Debug.Log("Anima start");
            isOn = true;
        }
        else if (isOn)
        {
            Debug.Log("Anima end");
            isOn = false;
        }
    }
}
