﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Database {
    [SerializeField] public int[] PadsInCounter = new int[4];
    [SerializeField] public int[] PadsOutCounter = new int[4];
    public float TimeInGame;
    public float[] TimeOnPad = new float[4];

    public Database(AnimationHandler variables)
    {
        PadsInCounter = variables.PadsInCounter;
        PadsOutCounter = variables.PadsOutCounter;
        TimeInGame = variables.TimeInGame;
        TimeOnPad = variables.TimeOnPad;
    }
}
