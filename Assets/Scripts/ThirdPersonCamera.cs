﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour {
    private float _rotationSpeed = 1;
    private Transform _target, _player;

    private float mouseX, mouseY;

    private void Start()
    {
        _player = GameObject.FindWithTag("Player").GetComponent<Transform>();
        _target = GameObject.FindWithTag("Target").GetComponent<Transform>();

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void LateUpdate()
    {
        CamController();
    }

    private void CamController()
    {
        mouseX += Input.GetAxis("Mouse X") * _rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * _rotationSpeed;

        mouseY = Mathf.Clamp(mouseY, -35, 60);

        this.transform.LookAt(_target);

        _target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
        _player.rotation = Quaternion.Euler(0, mouseX, 0);
    }
}
