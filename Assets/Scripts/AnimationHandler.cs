﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    [SerializeField] private Animator[] _animators;
    public int[] PadsInCounter = new int[4];
    public int[] PadsOutCounter = new int[4];
    public float[] TimeOnPad = new float[4];
    public float TimeInGame = 0;

    private bool isCourtineExecuting = false;

    private void Awake()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Anima");
        _animators = new Animator[objects.Length];

        for (int i = 0; i < objects.Length; i++)
        {
            PadsInCounter[i] = 0;
            PadsOutCounter[i] = 0;
            _animators[i] = objects[i].GetComponent<Animator>();
        }
        
        Database data = SaveDatabase.LoadPlayer();
        if (data != null)
        {
            PadsInCounter = data.PadsInCounter;
            PadsOutCounter = data.PadsOutCounter;
            TimeInGame = data.TimeInGame;
            TimeOnPad = data.TimeOnPad;
        }
    }

    private void Update()
    {
        TimeInGame += Time.deltaTime;
        StartCoroutine(SavingAfterTime(2));
    }

    IEnumerator SavingAfterTime(float time)
    {
        if (isCourtineExecuting)
            yield break;

        isCourtineExecuting = true;
        yield return new WaitForSeconds(time);
        SaveDatabase.SavePlayer(this.GetComponent<AnimationHandler>());
        isCourtineExecuting = false;
    }

    private void TurnOnAnimation(Animator animator)
    {
        animator.SetBool("ExecuteAnima", true);
    }
    private void TurnOffAnimation(Animator animator)
    {
        animator.SetBool("ExecuteAnima", false);
    }

    private void OnCollisionStay(Collision collision)
    {
        switch (collision.gameObject.name)
        {
            case "PinkPad":
                {
                    TimeOnPad[0] += Time.deltaTime;
                    break;
                }
            case "BluePad":
                {
                    TimeOnPad[1] += Time.deltaTime;
                    break;
                }
            case "RedPad":
                {
                    TimeOnPad[2] += Time.deltaTime;
                    break;
                }
            case "GreenPad":
                {
                    TimeOnPad[3] += Time.deltaTime;
                    break;
                }
        }  
    }

    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.name)
        {
            case "PinkPad":
                {
                    TurnOnAnimation(_animators[0]);
                    PadsInCounter[0]++;
                    break;
                }
            case "BluePad":
                {
                    TurnOnAnimation(_animators[1]);
                    PadsInCounter[1]++;
                    break;
                }
            case "RedPad":
                {
                    TurnOnAnimation(_animators[2]);
                    PadsInCounter[2]++;
                    break;
                }
            case "GreenPad":
                {
                    TurnOnAnimation(_animators[3]);
                    PadsInCounter[3]++;
                    break;
                }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        switch (collision.gameObject.name)
        {
            case "PinkPad":
                {
                    TurnOffAnimation(_animators[0]);
                    PadsOutCounter[0]++;
                    break;
                }
            case "BluePad":
                {
                    TurnOffAnimation(_animators[1]);
                    PadsOutCounter[1]++;
                    break;
                }
            case "RedPad":
                {
                    TurnOffAnimation(_animators[2]);
                    PadsOutCounter[2]++;
                    break;
                }
            case "GreenPad":
                {
                    TurnOffAnimation(_animators[3]);
                    PadsOutCounter[3]++;
                    break;
                }
        }
    }
}
