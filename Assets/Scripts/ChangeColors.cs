﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColors : MonoBehaviour
{
    [SerializeField] [Range(0.0f, 5.0f)] private float _speed = 1;
    [SerializeField] private Color _startColor;
    [SerializeField] private Color _endColor;
    private float _startTime;

    private void Start()
    {
        _startTime = Time.time;
    }

    void Update()
    {
        float duration = (Mathf.Sin(Time.time - _startTime) * _speed);
        GetComponent<Renderer>().material.color = Color.Lerp(_startColor, _endColor, duration);
    }
}
